package com.ukefu.webim.service.es;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ukefu.webim.web.model.OnlineUser;

public abstract interface OnlineUserESRepository extends  ElasticsearchRepository<OnlineUser, String>{
	public abstract OnlineUser findBySessionidAndOrgi(String paramString, String orgi);
	
	public abstract List<OnlineUser> findByUseridAndOrgi(String userid, String orgi);
	
	public abstract int countByUseridAndOrgi(String userid, String orgi);
	
	public abstract Page<OnlineUser> findByUseridAndOrgi(String userid, String orgi , Pageable page);
	
	public abstract OnlineUser findByOrgiAndSessionid(String orgi , String sessionid);
	
	public abstract Page<OnlineUser> findByOrgiAndStatusAndCreatetimeLessThan(String orgi , String status , long createtime , Pageable paramPageable);

	public abstract Page<OnlineUser> findByStatusAndCreatetimeLessThan(String status , long createtime , Pageable paramPageable);
	
	public abstract Page<OnlineUser> findByOrgiAndStatus(String orgi, String status , Pageable page);
	
	//@Query("select invitestatus , count(id) as users from OnlineUser where orgi = ?1 and status = ?2 group by invitestatus")
	//List<Object> findByOrgiAndStatus(String orgi ,String status);
	
}
