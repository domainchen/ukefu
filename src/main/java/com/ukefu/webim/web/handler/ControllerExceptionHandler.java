package com.ukefu.webim.web.handler;

import org.springframework.boot.autoconfigure.web.AbstractErrorController;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
@Controller
public class ControllerExceptionHandler extends AbstractErrorController {
    public ControllerExceptionHandler(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }
    @ExceptionHandler(value = Exception.class)
    public ModelAndView handleHtml(Exception ex) throws Exception {
    	return new ModelAndView("/error");
    }
	@Override
	public String getErrorPath() {
		return "/error.html";
	}
}
