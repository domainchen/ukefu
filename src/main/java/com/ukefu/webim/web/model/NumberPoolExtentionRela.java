package com.ukefu.webim.web.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;


/**
 * 号码 分机 关联表
 *
 */
@Entity
@Table(name = "uk_number_pool_extention_rela")
@org.hibernate.annotations.Proxy(lazy = false)
public class NumberPoolExtentionRela implements java.io.Serializable{

	private static final long serialVersionUID = -3206955623997356722L;

	@Id
	private String id;

	private String hostid;

	private String extentionid;

	private String numberpoolid;

	private String creater;
	private String updater;
	private Date createtime = new Date();
	private Date updatetime = new Date();
	
	private boolean datastatus ;//数据状态，是否已删除 0否 1是

	private String orgi;
	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHostid() {
		return hostid;
	}

	public void setHostid(String hostid) {
		this.hostid = hostid;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public String getUpdater() {
		return updater;
	}

	public void setUpdater(String updater) {
		this.updater = updater;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public boolean isDatastatus() {
		return datastatus;
	}

	public void setDatastatus(boolean datastatus) {
		this.datastatus = datastatus;
	}

	public String getExtentionid() {
		return extentionid;
	}

	public void setExtentionid(String extentionid) {
		this.extentionid = extentionid;
	}

	public String getNumberpoolid() {
		return numberpoolid;
	}

	public void setNumberpoolid(String numberpoolid) {
		this.numberpoolid = numberpoolid;
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
}
