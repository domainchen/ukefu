package com.ukefu.webim.web.model;

public class ReqlogWarningContent implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7981615404435256640L;

	private String warnlv ;
	private String warn ;
	private String action ;
	public String getWarnlv() {
		return warnlv;
	}
	public void setWarnlv(String warnlv) {
		this.warnlv = warnlv;
	}
	public String getWarn() {
		return warn;
	}
	public void setWarn(String warn) {
		this.warn = warn;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	
	
}
