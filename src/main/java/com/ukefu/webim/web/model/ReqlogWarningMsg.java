package com.ukefu.webim.web.model;

import java.util.Date;
import java.util.List;

public class ReqlogWarningMsg implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3037032517209004637L;
	private List<ReqlogWarningContent> content;
	private String msgusername;
	private String email;
	private String mobile;
	private Date warnDate ; 
	
	private String url;//操作url
	private String parameters;//传入参数
	private String ip;//ip地址
	private String username;
	private String userid;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Date getWarnDate() {
		return warnDate;
	}
	public void setWarnDate(Date warnDate) {
		this.warnDate = warnDate;
	}
	public String getMsgusername() {
		return msgusername;
	}
	public void setMsgusername(String msgusername) {
		this.msgusername = msgusername;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getParameters() {
		return parameters;
	}
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public List<ReqlogWarningContent> getContent() {
		return content;
	}
	public void setContent(List<ReqlogWarningContent> content) {
		this.content = content;
	}
	
	
	
}
